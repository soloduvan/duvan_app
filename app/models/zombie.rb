class Zombie < ApplicationRecord
    has_many:brains
    belongs_to:user
    VALID_EMAIL_REGEX = /\A[\w+\-.]+@[a-z\d\-.]+\.[a-z]+\z/i
    validates :name, presence: true
    validates :age, numericality:{only_integer: true, message: "Solo numeros enteros"}
    validates :email, format: { :with => VALID_EMAIL_REGEX , message: "El formato del correo es invalido" }
    validates :bio, length:{maximum: 100}
    mount_uploader :avatar, AvatarUploader
end